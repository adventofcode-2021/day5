const fs = require("fs");

class Point {
  constructor(x, y) {
    this.x = x;
    this.y = y;
  }
  equals(other) {
    return this.x == other.x && this.y == other.y;
  }
  toString() {
    return `[${this.x}, ${this.y}]`;
  }
  static FromString(xyString) {
    const xy = xyString.split(",");
    return new Point(parseInt(xy[0]), parseInt(xy[1]));
  }
  static getbetweenPoints(point1, point2) {
    const isDiagonal = point1.x != point2.x && point1.y != point2.y;
    if (isDiagonal) {
      return [];
    }
    const diffX = point2.x - point1.x;
    const distX = Math.abs(diffX);
    const stepX = diffX / distX || 0;

    const diffY = point2.y - point1.y;
    const distY = Math.abs(diffY);
    const stepY = diffY / distY || 0;

    const pointList = [];
    for (let i = 0; !pointList[pointList.length - 1]?.equals(point2); i++) {
      const point = new Point(point1.x + i * stepX, point1.y + i * stepY);
      pointList.push(point);
    }
    return pointList;
  }
}

const allPoints = fs
  .readFileSync("input-part1.txt")
  .toString()
  .trim()
  .split("\n")
  .map((x) => x.split(" -> ").map(Point.FromString))
  .map((x) => Point.getbetweenPoints(x[0], x[1]))
  .flatMap((x) => x);
const maxX = allPoints.sort((a, b) => a.x - b.x).reverse()[0].x + 1;
const maxY = allPoints.sort((a, b) => a.y - b.y).reverse()[0].y + 1;

const board = createBoard(maxY, maxX);

allPoints.map((x) => (board[x.y][x.x] += 1));

const greaterThanOne = board
  .flatMap((x) => x)
  .flatMap((y) => y)
  .filter((x) => x > 1).length;
console.log(greaterThanOne);

function createBoard(maxX, maxY) {
  const board = [];
  for (let i = 0; i < maxX; i++) {
    board[i] = new Array(maxY).fill(0);
  }
  return board;
}
